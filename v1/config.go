// SPDX-License-Identifier: Apache-2.0

package v1

import (
	"fmt"
	"os"

	"gopkg.in/yaml.v2"
)

// Config represents Corto configuration.
type Config struct {
	GoogleDriveId       string `yaml:"google_drive_id"`
	GoogleDocTemplateId string `yaml:"google_doc_template_id"`
}

// ReadConfig returns a Config struct from the contents of a YAML-formated file.
func ReadConfig(filename string) (*Config, error) {
	var data []byte
	var err error

	if data, err = os.ReadFile(filename); err != nil {
		return nil, fmt.Errorf("reading configuration from file: %w", err)
	}

	return unmarshalConfig(data)
}

func unmarshalConfig(data []byte) (*Config, error) {
	var config Config

	if err := yaml.Unmarshal(data, &config); err != nil {
		return nil, fmt.Errorf("unmarshalling yaml: %w", err)
	}

	return &config, nil
}
