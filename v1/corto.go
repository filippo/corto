// SPDX-License-Identifier: Apache-2.0

package v1

import (
	"bytes"
	"context"
	"fmt"
	"html/template"
	"time"

	"github.com/thought-machine/gonduit"
	"github.com/thought-machine/gonduit/entities"
	"github.com/thought-machine/gonduit/requests"
	"github.com/thought-machine/gonduit/responses"
	"gitlab.wikimedia.org/repos/sre/corto/templates"
	"google.golang.org/api/drive/v3"
	"google.golang.org/api/googleapi"
)

type Corto struct {
	phabConn  *gonduit.Conn
	gdriveSrv *drive.Service
	config    *Config
	// phabProject string # which phab project to search for incidents
	templates *template.Template
}

func NewCorto(config *Config, phab *gonduit.Conn, gdrive *drive.Service) (*Corto, error) {
	var err error
	var tmpl *template.Template

	if tmpl, err = template.ParseFS(templates.FS, "task.*.tmpl"); err != nil {
		return nil, fmt.Errorf("unable to parse templates: %w", err)
	}

	return &Corto{config: config, phabConn: phab, gdriveSrv: gdrive, templates: tmpl}, nil
}

// CreateIncident creates a new incident (Google Doc & Phabricator task), given an incident title.
func (c *Corto) CreateIncident(ctx context.Context, title string) (*Incident, error) {
	var descriptionBuf bytes.Buffer
	var err error

	if err = c.templates.ExecuteTemplate(&descriptionBuf, "task.description.tmpl", title); err != nil {
		return nil, fmt.Errorf("unable to create task description: %w", err)
	}

	var PHID string
	var task *entities.ManiphestTask

	if PHID, err = c.createTask(title, descriptionBuf.String()); err != nil {
		return nil, fmt.Errorf("unable to create task: %w", err)
	}

	if task, err = c.findTaskByPHID(PHID); err != nil {
		return nil, fmt.Errorf("unable to find task: %w", err)
	}

	fmt.Printf("Phab URI: %s\n", task.URI)

	var gdoc *drive.File

	if gdoc, err = c.createTmplGdoc(fmt.Sprintf("%s T%s", title, task.ID)); err != nil {
		return nil, fmt.Errorf("unable to retrieve files: %w", err)
	}

	fmt.Printf("Gdoc URI: %s\n", gdoc.WebViewLink)

	// TODO: Add gdoc to phab task as an attachment
	return &Incident{
		Id:         task.ID,
		Status:     Open,
		Started_at: time.Time(task.DateCreated),
		Title:      task.Title,
		Public:     false,
	}, nil
}

func (c *Corto) ListOpen(ctx context.Context) (bool, error) {
	// TODO list all open incidents in Phabricator
	return true, nil
}

// GetIncident loads an existing Incident from its task ID
func (c *Corto) GetIncident(ctx context.Context, task_id string) (*Incident, error) {
	var task *entities.ManiphestTask
	var err error

	if task, err = c.findTaskByID(task_id); err != nil {
		return nil, fmt.Errorf("unable to find task for id %s: %w", task_id, err)
	}

	return c.incidentFromTask(task)
}

func (c *Corto) incidentFromTask(task *entities.ManiphestTask) (*Incident, error) {
	var status status
	var start, end time.Time
	var public bool

	start = time.Time(task.DateCreated)

	if !task.IsClosed {
		status = Open
	} else {
		status = Closed
		// TODO likely not correct, no DateClosed present in ManiphestTask
		end = time.Time(task.DateModified)
	}

	public = true // TODO parse from task policy? not in ManiphestTask

	return &Incident{
		Id:         task.ID,
		Started_at: start,
		Ended_at:   end,
		Title:      task.Title,
		Status:     status,
		Public:     public,
	}, nil
}

func (c *Corto) ResolveIncident(ctx context.Context, PHID string) (bool, error) {
	// task, err := c.findTaskByPHID(PHID)
	// if err != nil {
	// 	return false, fmt.Errorf("Unable to find task: %w", err)
	// }
	// TODO resolve task
	return true, nil
}

func (c *Corto) TitleIncident(ctx context.Context, PHID string, new_title string) (bool, error) {
	// task, err := c.findTaskByPHID(PHID)
	// if err != nil {
	// 	return false, fmt.Errorf("Unable to find task: %w", err)
	// }
	// TODO change title
	return true, nil
}

func (c *Corto) UpdateIC(ctx context.Context, PHID string, new_ic string) (bool, error) {
	// task, err := c.findTaskByPHID(PHID)
	// if err != nil {
	// 	return false, fmt.Errorf("Unable to find task: %w", err)
	// }
	// TODO set IC
	return true, nil
}

// TODO: fill in templated values in doc after copying, e.g. <NAME>
func (c *Corto) createTmplGdoc(gdocTitle string) (*drive.File, error) {
	var gdoc *drive.File
	var err error

	gdoc = &drive.File{
		Name:    gdocTitle,
		Parents: []string{c.config.GoogleDriveId},
	}

	fileSrv := c.gdriveSrv.Files
	call := fileSrv.Copy(c.config.GoogleDocTemplateId, gdoc)
	if gdoc, err = call.Do(); err != nil {
		return nil, err
	}

	// Re-get our file, so we can grab the webViewLink field, alternatively, we could
	// just construct the url ourselves and hope the format never changes.
	f := []googleapi.Field{"webViewLink"}
	if gdoc, err = fileSrv.Get(gdoc.Id).Fields(f...).Do(); err != nil {
		return nil, err
	}

	return gdoc, nil
}

// findTaskByPHID returns the Phabricator task with the given PHID string
func (c *Corto) findTaskByPHID(PHID string) (*entities.ManiphestTask, error) {
	var err error
	var response *responses.ManiphestQueryResponse

	request := requests.ManiphestQueryRequest{PHIDs: []string{PHID}}
	if response, err = c.phabConn.ManiphestQuery(request); err != nil {
		return nil, err
	}

	for _, res := range *response {
		return res, nil
	}

	return nil, fmt.Errorf("no task with PHID %s found", PHID)
}

// findTaskByID returns the Phabricator task with the given ID string
func (c *Corto) findTaskByID(id string) (*entities.ManiphestTask, error) {
	var err error
	var response *responses.ManiphestQueryResponse

	request := requests.ManiphestQueryRequest{IDs: []string{id}}
	if response, err = c.phabConn.ManiphestQuery(request); err != nil {
		return nil, err
	}

	for _, res := range *response {
		return res, nil
	}
	return nil, fmt.Errorf("no task with id %s found", id)
}

func (c *Corto) createTask(title, taskBody string) (string, error) {
	var err error
	var request = requests.EditRequest{Transactions: []entities.Transaction{
		{Type: "title", Value: title},
		{Type: "description", Value: taskBody},
		{Type: "status", Value: "open"},
	}}
	var response *responses.EditResponse

	if response, err = c.phabConn.ManiphestEdit(request); err != nil {
		return "", err
	}

	return string(response.Object.PHID), nil
}
