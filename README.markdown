Corto
=====

It's short.


## Code conventions

### Named return parameters

Go allows you to supply named return parameters in the function
signature or definition.  When present, these variables do not need to
be declared within the function body, and a simple return statement (a
naked return) automatically returns the named return parameters.  Use
of named return parameters has the potential to be confusing though,
so as a general rule: If you use them it's probably best to standardize
and use them everywhere.  And if you do use them, it's probably best to
adopt the convention of always returning the named parameters (naked or
otherwise).

For Corto, we agreed to standardize on NOT using named return
parameters.

### Error handling

A *very* common pattern in Go is to invoke a function that returns
multiple results, one of which is of type `error` that is expected to be
non-nil when there are failures.  For example:

```golang
    res, err := divideNumbers(4, 0)
    if err != nil {
        log.Fatal("Oh no:", err)
    }

    fmt.Println("Result is:", res)
```

An alternative is to combine the assignment with the expression (an
initialization statement):

```golang
    var err error
    var res int

    if res, err = divideNumbers(4, 0); err != nil {
        log.Fatal("Oh no:", err)
    }

    fmt.Println("Result is:", res)
```

This can read a bit better (it looks closer to the exception-handling seen
in other languages).  One caveat with this however, is that using type
inference here would limit the scope of the variables to the enclosing
block.  To make use of a result outside the block, it needs to be
pre-delcared (in the example above, they both must be).  This usually
contributes to readability anyway by making scope (and types) more obvious
(explicit v implicit).

For Corto: Initialization statements can improve readability, so consider
them *preferred*, and use them whenever it makes sense.