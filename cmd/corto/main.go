// SPDX-License-Identifier: Apache-2.0

package main

import (
	"context"
	"log"
	"os"
	"strings"

	"github.com/thought-machine/gonduit"
	"github.com/thought-machine/gonduit/core"
	corto "gitlab.wikimedia.org/repos/sre/corto/v1"
	"google.golang.org/api/drive/v3"
	"google.golang.org/api/option"
)

func main() {

	phabTokenBytes, err := os.ReadFile("phab-token.txt")
	if err != nil {
		log.Fatalf("Unable to read phab token: %v", err)
	}

	phabToken := strings.TrimSuffix(string(phabTokenBytes), "\n")

	phabConn, err := gonduit.Dial(
		"https://phabricator.wikimedia.org",
		&core.ClientOptions{APIToken: phabToken},
	)
	if err != nil {
		log.Fatalf("Unable to connect to phab: %v", err)
	}

	ctx := context.Background()
	gdriveSrv, err := drive.NewService(ctx, option.WithCredentialsFile("gdrive-creds.json"))
	if err != nil {
		log.Fatalf("Unable to obtain Google Drive client: %v", err)
	}

	var cfg *corto.Config

	if cfg, err = corto.ReadConfig("config.yaml.sample"); err != nil {
		log.Fatalf("Unable to load configuration from file: %v", err)
	}

	c, _ := corto.NewCorto(cfg, phabConn, gdriveSrv)

	incident, err := c.CreateIncident(ctx, "CORTOTEST3")
	if err != nil {
		log.Fatalf("Unable to create incident: %v", err)
	}

	log.Printf("Created incident %v", incident)
}
