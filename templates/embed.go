// SPDX-License-Identifier: Apache-2.0

package templates

import "embed"

//go:embed all:*.tmpl
var FS embed.FS
